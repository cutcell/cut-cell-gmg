// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#define DUNE_ISTL_WITH_CHECKING 1

#include <iostream>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/common/parametertreeparser.hh>

/**
   We try to implement the following steps:
   a) diffuse interface approximation
   b) "subscale" diffuse interface
   c) cut-cell as diffuse interface
*/

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>

#include <dune/pdelab/stationary/linearproblem.hh>

#include <dune/cutcell/diffuse.hh>
#include <dune/cutcell/diffusemodel.hh>
#include <dune/cutcell/cutfemmodel.hh>

#include <dune/udg/io/refinedvtkwriter.hh>
#include <dune/udg/io/vtkfunction.hh>

#include <dune/cutcell/gmg.hh>
#include <dune/cutcell/interpolation.hh>
#include <dune/cutcell/solver.hh>
#include <dune/cutcell/prolongationassembler.hh>

using namespace Dune;
template<typename V>
using Native = PDELab::Backend::Native<V>;

template<typename Grid, typename LSET>
void diffuse(const Grid & grid, LSET && lset, ParameterTree & param, double h)
{
  // configuration
  std::string prectype = param.get("diffuse-prec","GMG");
  int hierarchy = param.get("hierarchy",grid.maxLevel()); // how deep is the GMG hierarchy?
  double epsilon = param.get("diffuse-eps",2.0)*h; // epsilon on course level

  // using GV = Grid::LeafGridView;
  using GV = typename Grid::LevelGridView;
  using Model = CutCell::DiffuseModel<GV>;
  constexpr int intorder = 2;
  auto gv = grid.levelGridView(grid.maxLevel());
  Model model(gv,lset,epsilon/(1<<grid.maxLevel()),intorder);

  int VERSION = param.get("diffuse-v",1);

  // typedefs
  using V = typename Model::V;
  using X = Native<V>;
  using GO = typename Model::GO;
  using M = typename GO::Jacobian;

  int levels = hierarchy+1;
  int coarselevel = grid.maxLevel() - hierarchy;

  // create operator hierarchy
  std::vector<std::shared_ptr<GV>> gvHierarchy(levels);
  std::vector<std::shared_ptr<Model>> modelHierarchy(levels);
  std::vector<unsigned int> sizes(levels);

  std::vector<std::shared_ptr<M>> prolongationmatrices(levels);
  std::vector<std::shared_ptr<LinearOperator<X,X>>> prologongation(levels);
  std::vector<std::shared_ptr<LinearOperator<X,X>>> restriction(levels);

  std::vector<std::shared_ptr<M>> matrices(levels);
  std::vector<std::shared_ptr<Preconditioner<X,X>>> smoother(levels);
  std::vector<std::shared_ptr<LinearOperator<X,X>>> operators(levels);

  std::shared_ptr<InverseOperator<X,X>> coarseSolver;

  if (prectype == "GMG") {
    for (int i=0; i<levels; i++)
    {
      using Smoother = SeqSSOR<Native<M>,X,X>;
      auto gv = grid.levelGridView(i+coarselevel);
      // model
      double eps = epsilon/(1<<i+coarselevel);
      if (param.get("epsilon","same")=="same")
        eps = 2*h/(1<<grid.maxLevel());
      modelHierarchy[i] = std::make_shared<Model>(gv,lset,eps,intorder);
      auto & m = *modelHierarchy[i];
      m.selectVersion(VERSION);
      sizes[i] = m.gfs().size();
      // matrices
      matrices[i] = std::make_shared<M>(m.go());
      auto v = m.emptyVector();
      m.go().jacobian(v,*matrices[i]);
      int pre_post = 2;
      auto & A = PDELab::Backend::native(*matrices[i]);
      // smoother
      smoother[i] = std::make_shared<Smoother>(A, pre_post, 1.0);
      // using ILU = SeqILU<Native<M>,X,X>;
      // smoother[i] = std::make_shared<ILU>(A, 0, 1.0);
      // operators
      operators[i] = std::make_shared<MatrixAdapter<Native<M>,X,X>>(A);
    }
    // create prologation and restriction operators...
    for (int i=1; i<levels; i++)
    {
#if 0
      using Inter = CutCell::InterpolationOperator<Model,Model>;
      // prologongation[i] : V[i-1] -> V[i]
      prologongation[i] = std::make_shared<Inter>(*modelHierarchy[i-1],*modelHierarchy[i]);
      // restriction[i] : V[i+1] -> V[i]
      restriction[i-1] = std::make_shared<Inter>(*modelHierarchy[i],*modelHierarchy[i-1]);
#else
      // prologongation[i] : V[i-1] -> V[i]
      using PAssembler = PDELab::Multigrid::ProlongationAssembler<typename Model::GFS, typename Model::MBE, double>;
      PAssembler passembler(modelHierarchy[i]->gfs(), modelHierarchy[i-1]->gfs());
      prolongationmatrices[i] = std::make_shared<M>(passembler);
      *prolongationmatrices[i] = 0.0;
      passembler.assemble(*prolongationmatrices[i]);
      prologongation[i] = std::make_shared<TransposedMatrixAdapter<Native<M>,X,X>>(PDELab::Backend::native(*prolongationmatrices[i]));
      std::cout << PDELab::Backend::native(*prolongationmatrices[i]).N()
                << " x " << PDELab::Backend::native(*prolongationmatrices[i]).M()
                << std::endl;
      // restriction[i] : V[i+1] -> V[i]
      restriction[i-1] = std::make_shared<MatrixAdapter<Native<M>,X,X>>(PDELab::Backend::native(*prolongationmatrices[i]));
#endif
    }
    // create coarse solver
    coarseSolver = std::make_shared<SuperLU<Native<M>>>(PDELab::Backend::native(*matrices[0]));
  }

  // create solution vectors...
  std::vector<V> u(3, model.emptyVector());

  // solver ...
  for (int i=0; i<1; i++)
  {
    // select model
    model.selectVersion(VERSION);
    // assemble matrix (for preconditioner)
    M A(model.go());
    model.go().jacobian(u[i],A);
    // setup linear solver
    using ILU = SeqILU<Native<M>,X,X>;
    using GMG = CutCell::GMG<X>;
    using CG = CGSolver<X>;
    using LS = PDELab::ISTL::GenericSolver<GO>;
    using PREC = Dune::Preconditioner<X,X>;
    std::shared_ptr<PREC> prec(nullptr);
    if (prectype == "ILU")
      prec = std::make_shared<ILU>(PDELab::Backend::native(A), 0, 1.0);
    if (prectype == "GMG")
      prec = std::make_shared<GMG>(
        sizes,
        prologongation,
        restriction,
        smoother,
        coarseSolver,
        operators
        );
    LS ls(MetaType<CG>(), prec, 100, 3);
    // solve problem
    using Solver = PDELab::StationaryLinearProblemSolver<GO,LS,V>;
    Solver solver(model.go(),ls,u[i],1e-8);
    solver.apply();
  }

  // plot
  VTKWriter<GV> vtkwriter(model.gv(),VTK::conforming);
  model.addDomainToVTKWriter(vtkwriter);
  for (int i=0; i<3; i++)
  {
    model.selectVersion(i+1);
    PDELab::addSolutionToVTKWriter(vtkwriter,model.gfs(),u[i]);
  };
  vtkwriter.write("cutcell-gmg-diffuse",Dune::VTK::appendedraw);
}


template<typename GV, typename LGV, typename LSET>
void cutfem(const GV & gv, const LGV & lgv, LSET && lset, ParameterTree & param, double h)
{
  using Model = CutCell::CutFEMModel<GV,LGV>;
  using DiffuseModel = CutCell::DiffuseModel<GV>;
  Model model(gv,lgv,lset,h);
  DiffuseModel diffusemodel(gv,lset,h/(1<<gv.grid().maxLevel()),2);
  diffusemodel.selectVersion(param.get("cutfem-diff-v",1));
  int hierarchy = param.get("hierarchy",gv.grid().maxLevel()); // how deep is the GMG hierarchy?
  int levels = std::max(2,hierarchy+1);
  int coarselevel = gv.grid().maxLevel() - hierarchy;

  std::string prectype = param.get("cutfem-prec","TL"); // TL, GMG, ILU, AMG

  using V = typename Model::V;
  using X = Native<V>;
  using GFS = typename Model::GFS;
  using GO = typename Model::GO;
  using ST = typename Model::ST;
  using M = typename GO::Jacobian;
  using DM = typename DiffuseModel::GO::Jacobian;

  // create solution vector...
  V u = model.emptyVector();

  // solver ...
  // using LS = PDELab::ISTLBackend_SEQ_CG_ILU0; // AMG_SSOR<GO>;
  // assemble matrix (for preconditioner)
  M A(model.go());
  DM dA(diffusemodel.go());
  model.go().jacobian(u,A);
  // setup linear solver
  using ILU = SeqILU<Native<M>,X,X>;
  using GMG = CutCell::GMG<X>;
  using CG = CGSolver<X>;
  using LS = PDELab::ISTL::GenericSolver<GO>;
  using PREC = Dune::Preconditioner<X,X>;
  std::shared_ptr<PREC> prec(nullptr);
  if (prectype == "ILU")
    prec = std::make_shared<ILU>(PDELab::Backend::native(A), 0, 1.0);
  using DGFS = typename DiffuseModel::GFS;
  using GFS = typename Model::GFS;
  typedef Dune::PDELab::ISTL::BCRSMatrixBackend<> MBE;
  typedef Dune::PDELab::EmptyTransformation CC;
  using CGTODGLOP = Dune::PDELab::CG2DGProlongation;
  typedef Dune::PDELab::GridOperator<GFS,DGFS,CGTODGLOP,MBE,double,double,double,CC,CC> PGO;
  typedef typename PGO::Jacobian PMatrix; // wrapped ISTL prolongation matrix
  using P = Native<PMatrix>;     // ISTL prolongation matrix

  // create operator hierarchy
  std::vector<std::shared_ptr<GV>> gvHierarchy(levels);
  std::vector<std::shared_ptr<Model>> modelHierarchy(levels);
  std::vector<unsigned int> sizes(levels);

  std::vector<std::shared_ptr<PMatrix>> prolongationmatrices(levels);
  std::vector<std::shared_ptr<LinearOperator<X,X>>> prologongation(levels);
  std::vector<std::shared_ptr<LinearOperator<X,X>>> restriction(levels);

  std::vector<std::shared_ptr<Preconditioner<X,X>>> smoother(levels);
  std::vector<std::shared_ptr<LinearOperator<X,X>>> operators(levels);

  std::shared_ptr<InverseOperator<X,X>> coarseSolver;

  CGTODGLOP cgtodglop;
  PGO pgo(model.gfs(),diffusemodel.gfs(),cgtodglop,MBE(10));

  if (prectype == "TL") // two-level solver
  {
    using Smoother = SeqSSOR<Native<M>,X,X>;

    // create coarse solver
    auto tmp = diffusemodel.emptyVector();
    diffusemodel.go().jacobian(tmp,dA);
    coarseSolver = std::make_shared<SuperLU<Native<M>>>(PDELab::Backend::native(dA));

    // level 1
    {
      auto & m = model;
      auto & nA = PDELab::Backend::native(A);
      sizes[1] = m.gfs().size();
      int pre_post = 2;
      smoother[1] = std::make_shared<Smoother>(nA, pre_post, 1.0);
      operators[1] = std::make_shared<MatrixAdapter<Native<M>,X,X>>(nA);
    }
    // level 0
    {
      auto & m = diffusemodel;
      auto & nA = PDELab::Backend::native(dA);
      sizes[0] = m.gfs().size();
      int pre_post = 2;
      smoother[0] = std::make_shared<Smoother>(nA, pre_post, 1.0);
      operators[0] = std::make_shared<MatrixAdapter<Native<M>,X,X>>(nA);
    }
    // create prologation and restriction operators...
    {
      int i = 1;
      // prologongation[i] : V[i-1] -> V[i]
      prolongationmatrices[i] = std::make_shared<PMatrix>(pgo);
      *prolongationmatrices[i] = 0.0;
      // auto tmp = model.emptyVector();
      // pgo.jacobian(tmp,*prolongationmatrices[i]);
      prologongation[i] = std::make_shared<TransposedMatrixAdapter<P,X,X>>(PDELab::Backend::native(*prolongationmatrices[i]));
      // restriction[i] : V[i+1] -> V[i]
      restriction[i-1] = std::make_shared<MatrixAdapter<P,X,X>>(PDELab::Backend::native(*prolongationmatrices[i]));
    }

    prec = std::make_shared<GMG>(
      sizes,
      prologongation,
      restriction,
      smoother,
      coarseSolver,
      operators
      );
  }
  if (prectype == "GMG")
  {
//     for (int i=0; i<levels; i++)
//     {
//       using Smoother = SeqSSOR<Native<M>,X,X>;
//       auto gv = gv.grid().levelGridView(i+coarselevel);
//       // model
//       modelHierarchy[i] = std::make_shared<Model>(gv,lset,epsilon/(1<<i+coarselevel),intorder);
//       auto & m = *modelHierarchy[i];
//       sizes[i] = m.gfs().size();
//       // matrices
//       matrices[i] = std::make_shared<M>(m.go());
//       auto v = m.emptyVector();
//       m.go().jacobian(v,*matrices[i]);
//       int pre_post = 2;
//       auto & A = PDELab::Backend::native(*matrices[i]);
//       // smoother
//       smoother[i] = std::make_shared<Smoother>(A, pre_post, 1.0);
//       // using ILU = SeqILU<Native<M>,X,X>;
//       // smoother[i] = std::make_shared<ILU>(A, 0, 1.0);
//       // operators
//       operators[i] = std::make_shared<MatrixAdapter<Native<M>,X,X>>(A);
//     }
//     // create prologation and restriction operators...
//     for (int i=1; i<levels; i++)
//     {
// #if 0
//       using Inter = CutCell::InterpolationOperator<Model,Model>;
//       // prologongation[i] : V[i-1] -> V[i]
//       prologongation[i] = std::make_shared<Inter>(*modelHierarchy[i-1],*modelHierarchy[i]);
//       // restriction[i] : V[i+1] -> V[i]
//       restriction[i-1] = std::make_shared<Inter>(*modelHierarchy[i],*modelHierarchy[i-1]);
// #else
//       // prologongation[i] : V[i-1] -> V[i]
//       using PAssembler = PDELab::Multigrid::ProlongationAssembler<typename Model::GFS, typename Model::MBE, double>;
//       PAssembler passembler(modelHierarchy[i]->gfs(), modelHierarchy[i-1]->gfs());
//       prolongationmatrices[i] = std::make_shared<M>(passembler);
//       *prolongationmatrices[i] = 0.0;
//       passembler.assemble(*prolongationmatrices[i]);
//       prologongation[i] = std::make_shared<TransposedMatrixAdapter<Native<M>,X,X>>(PDELab::Backend::native(*prolongationmatrices[i]));
//       std::cout << PDELab::Backend::native(*prolongationmatrices[i]).N()
//                 << " x " << PDELab::Backend::native(*prolongationmatrices[i]).M()
//                 << std::endl;
//       // restriction[i] : V[i+1] -> V[i]
//       restriction[i-1] = std::make_shared<MatrixAdapter<Native<M>,X,X>>(PDELab::Backend::native(*prolongationmatrices[i]));
// #endif
//     }
//     // create coarse solver
//     coarseSolver = std::make_shared<SuperLU<Native<M>>>(PDELab::Backend::native(*matrices[0]));

//     prec = std::make_shared<GMG>(
//       sizes,
//       prologongation,
//       restriction,
//       smoother,
//       coarseSolver,
//       operators
//       );
  }
  LS ls(MetaType<CG>(), prec, 500, 3);
  using Solver = PDELab::StationaryLinearProblemSolver<GO,LS,V>;
  Solver solver(model.go(),ls,u,1e-8);
  solver.apply();

  // plot
  RefinedVtkWriter<GV, ST, double> refinedvtk(gv, model.triangulation());
  using UVGF = UDG::UnfittedVTKGridFunction<GFS,V,ST>;
  using DVGF = UDG::DomainIndexUnfittedVTKGridFunction<GV>;
  refinedvtk.addVertexData(new UVGF("u",model.gfs(),u,model.triangulation()));
  refinedvtk.write("cutcell-gmg-test", VTK::appendedraw, 255, writeVolume);

  // plot extended
  VTKWriter<GV> vtkwriter(gv,VTK::conforming);
  PDELab::addSolutionToVTKWriter(vtkwriter,model.gfs(),u);
  vtkwriter.write("cutcell-gmg-extended",Dune::VTK::appendedraw);
}

int main(int argc, char** argv)
{
  try{
    // Maybe initialize MPI
    MPIHelper& helper = MPIHelper::instance(argc, argv);

    Dune::ParameterTree param;
    Dune::ParameterTreeParser::readOptions(argc,argv,param);

    constexpr int dim = 2;
    using Grid = YaspGrid<dim>;

    // create grid & gridview
    int refine = param.get("refine",1);
    std::cout << "Cut Cell GMG test, " << refine << " levels" << std::endl;
    int N = param.get("N",16);
    FieldVector<double,dim> Len(1.0);
    std::array<int,dim> s;
    std::fill(s.begin(), s.end(), N);
    double h = 1.0/double(N);
    Grid grid(Len,s);
    grid.globalRefine(refine);
    auto lgv = grid.levelGridView(grid.maxLevel());    // level-set grid view

    int cclevel = param.get("cutfem-level",refine);
    auto ccgv = grid.levelGridView(cclevel); // cut-cell grid view

    // level set
    FieldVector<double,dim> center{0.5,0.5};
    double R = 0.35;
    auto lset = [&center,&R](auto x)
      {
        return (x - center).two_norm() - R;
      };
    // diffuse(grid,lset,param,h);
    cutfem(ccgv,lgv,lset,param,h);

    return 0;
  }
  catch (Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
