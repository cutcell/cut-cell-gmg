// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_CUTCELL_MODEL_HH
#define DUNE_CUTCELL_MODEL_HH

#include "convection_diffusion_dg_operator.hh"

namespace Dune {
  namespace CutCell {

    template<typename GV, typename RF>
    class TestProblem
    {
    public:
      typedef PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;
      typedef PDELab::ConvectionDiffusionParameterTraits<GV, typename GV::ctype> Traits;

      //! tensor diffusion coefficient
      template<typename E>
      typename Traits::PermTensorType
      A (const E& e, const typename Traits::DomainType& x) const
      {
        typename Traits::PermTensorType I;
        for (std::size_t i=0; i<Traits::dimDomain; i++)
          for (std::size_t j=0; j<Traits::dimDomain; j++)
            I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! tensor diffusion coefficient
      template<typename IG>
      typename Traits::PermTensorType
      A (const IG& i, const typename Traits::IntersectionDomainType& x, int side) const
      {
        typename Traits::PermTensorType I;
        for (std::size_t i=0; i<Traits::dimDomain; i++)
          for (std::size_t j=0; j<Traits::dimDomain; j++)
            I[i][j] = (i==j) ? 1 : 0;
        return I;
      }

      //! velocity field
      template<typename E>
      typename Traits::RangeType
      b (const E& e, const typename Traits::DomainType& x) const
      {
        typename Traits::RangeType v(0.0);
        return v;
      }

      //! sink term
      template<typename E>
      typename Traits::RangeFieldType
      c (const E& e, const typename Traits::DomainType& x) const
      {
        return 0.0;
      }

      //! source term
      typename Traits::RangeFieldType
      f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 1.0;
      }

      //! boundary condition type function
      template<typename I>
      BCType
      bctype (const I& is, const typename Traits::IntersectionDomainType& x) const
      {
        return PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
      }

      //! Dirichlet boundary condition value
      template<typename I>
      typename Traits::RangeFieldType
      g (const I& i, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }

      //! Neumann boundary condition
      template<typename I>
      typename Traits::RangeFieldType
      j (const I& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }

      //! outflow boundary condition
      template<typename I>
      typename Traits::RangeFieldType
      o (const I& is, const typename Traits::IntersectionDomainType& x) const
      {
        return 0.0;
      }
    };

  }
}


#endif // DUNE_CUTCELL_MODEL_HH
