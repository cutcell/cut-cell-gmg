#ifndef CUTCELL_DIFFUSE_HH
#define CUTCELL_DIFFUSE_HH

#include <dune/common/power.hh>

#include <dune/pdelab/common/quadraturerules.hh>

#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/defaultimp.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>

namespace Dune {

namespace CutCell {

  template<typename GV, typename RF>
  class DiffuseInterfaceParameters
  {
  public:
    using GridView = GV;
    using RangeField = RF;
    static constexpr int dim = GV::dimension;
    using Domain = FieldVector<typename GV::ctype, dim>;
    using Entity = typename GV::template Codim<0>::Entity;
    using LocalFunction = Functions::LocalFunction< RF(Domain), Entity>;

    template<typename F1, typename F2, typename F3, typename F4, typename F5>
    DiffuseInterfaceParameters(F1 && f1, F2 && f2, F3 && f3, F4 && f4, F5 && f5,
      double e, int i = 2) :
      Phi(f1), Xi(f2), B(f3), g(f4), f(f5),
      eps(e), intorder(i) {};

    LocalFunction Phi;
    LocalFunction Xi;
    LocalFunction B;
    LocalFunction g;
    LocalFunction f;

    double eps;
    int intorder;

    void bind(const Entity & e)
    {
      Phi.bind(e);
      Xi.bind(e);
      B.bind(e);
      g.bind(e);
      f.bind(e);
    }

    void unbind()
    {
      Phi.unbind();
      Xi.unbind();
      B.unbind();
      g.unbind();
      f.unbind();
    }
  };

  template<typename T, typename FiniteElementMap>
  class DiffuseInterfacePoisson :
    public Dune::PDELab::NumericalJacobianApplyVolume<DiffuseInterfacePoisson<T,FiniteElementMap> >,
    public Dune::PDELab::NumericalJacobianApplyBoundary<DiffuseInterfacePoisson<T,FiniteElementMap> >,
    public Dune::PDELab::NumericalJacobianVolume<DiffuseInterfacePoisson<T,FiniteElementMap> >,
    public Dune::PDELab::NumericalJacobianBoundary<DiffuseInterfacePoisson<T,FiniteElementMap> >,
    public Dune::PDELab::FullVolumePattern,
    public Dune::PDELab::LocalOperatorDefaultFlags,
    public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename T::RangeField>
  {
  public:
    // pattern assembly flags
    enum { doPatternVolume = true };
    // residual assembly flags
    enum { doAlphaVolume = true };

    using GridView = typename T::GridView;

    DiffuseInterfacePoisson (T& param_, int v = 1)
      : param(param_), version(v)
    {
    }

    // volume integral depending on test and ansatz functions
    template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
    void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
    {
      // Define types
      using RF = typename LFSU::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeFieldType;
      using size_type = typename LFSU::Traits::SizeType;

      // dimensions
      const int dim = EG::Entity::dimension;

      // Reference to cell
      const auto& cell = eg.entity();

      // Get geometry
      auto geo = eg.geometry();

      // bind parameter functions
      param.bind(eg.entity());

      // Initialize vectors outside for loop
      std::vector<Dune::FieldVector<RF,dim> > gradphi(lfsu.size());
      Dune::FieldVector<RF,dim> gradu(0.0);
      Dune::FieldVector<RF,dim> Agradu(0.0);

      // Transformation matrix
      typename EG::Geometry::JacobianInverseTransposed jac;

      // loop over quadrature points
      for (const auto& ip : quadratureRule(geo,param.intorder))
      {
        // evaluate basis functions
        auto& phi = cache.evaluateFunction(ip.position(),lfsu.finiteElement().localBasis());

        // evaluate u
        RF u=0.0;
        for (size_type i=0; i<lfsu.size(); i++)
          u += x(lfsu,i)*phi[i];

        // evaluate gradient of shape functions (we assume Galerkin method lfsu=lfsv)
        auto& js = cache.evaluateJacobian(ip.position(),lfsu.finiteElement().localBasis());

        // transform gradients of shape functions to real element
        jac = geo.jacobianInverseTransposed(ip.position());
        for (size_type i=0; i<lfsu.size(); i++)
          jac.mv(js[i][0],gradphi[i]);

        // compute gradient of u
        gradu = 0.0;
        for (size_type i=0; i<lfsu.size(); i++)
          gradu.axpy(x(lfsu,i),gradphi[i]);

        // evaluate velocity field, sink term and source term
        auto Phi = param.Phi(ip.position());
        auto Xi = param.Xi(ip.position());
        auto B = param.B(ip.position());
        auto g = param.g(ip.position());
        auto f = param.f(ip.position());
        // quick and dirty trick to compute normal vector...
        auto normal = geo.global(ip.position()) - FieldVector<double,dim>(0.5);

        RF factor = ip.weight() * geo.integrationElement(ip.position());
        for (size_type i=0; i<lfsu.size(); i++)
        {
          switch (version)
          {
            case 1: // Eq 2.21
              // integrate -Xi*(grad u)*(grad phi_i) - eps^-3 * (1-Xi) * (u-g) - f*phi_i = 0
              r.accumulate(lfsu,i, ( - Xi*(gradu*gradphi[i]) - (1.0-Xi)/Power<3>::eval(param.eps)*(u-g)*phi[i] - -f*Xi*phi[i] )*factor);
              break;
            case 2: // Eq 2.22
              // integrate -(grad u)*(grad phi_i) - eps^-3 * B(Xi) * (u-g) + f*phi_i = 0
              r.accumulate(lfsu,i, ( - (gradu*gradphi[i]) - B/Power<3>::eval(param.eps)*(u-g)*phi[i] + f*Xi*phi[i] )*factor);
              break;
            case 3: // Eq 2.22 + consistency term
              // integrate -(grad u)*(grad phi_i) - eps^-3 * B(Xi) * (u-g) - f*phi_i = 0
              r.accumulate(lfsu,i, ( - (gradu*gradphi[i])
                  + B/param.eps*(gradu*normal)*phi[i]
                  + B/param.eps*(gradphi[i]*normal)*u
                  - B/Power<3>::eval(param.eps)*(u-g)*phi[i] + f*Xi*phi[i] )*factor);
              break;
            default:
              DUNE_THROW(Exception, "unsupported diffuse interface formulation " << version);
          }
        }
      }
    }

    void selectVersion(int v)
    {
      version = v;
      std::cout << "Select DiffuseInterface Formulation " << version << std::endl;
    }

  private:
    T& param;
    int version;
    using LocalBasisType = typename FiniteElementMap::Traits::FiniteElementType::Traits::LocalBasisType;
    Dune::PDELab::LocalBasisCache<LocalBasisType> cache;
  };

} // end namespace CutCell

} // end namespace Dune

#endif // CUTCELL_DIFFUSE_HH
