#ifndef CUTCELL_CUTFEM_MODEL_HH
#define CUTCELL_CUTFEM_MODEL_HH

#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>

#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/backend/istl.hh>

#include <dune/localfunctions/lagrange/q1.hh>

#include <dune/udg/pdelab/subtriangulation.hh>
#include <dune/udg/pdelab/finiteelementmap.hh>
#include <dune/udg/pdelab/multiphaseoperator.hh>
#include <dune/udg/pdelab/operator.hh>
#include <dune/udg/simpletpmctriangulation.hh>
#include <dune/udg/vtriangulation.hh>

#include "model.hh"
#include "convection_diffusion_dg_operator.hh"
#include "edge_norm_provider.hh"
#include "penalty_flux_weighting.hh"

namespace Dune {

namespace CutCell {

template<typename _GV, typename LGV>
class CutFEMModel
{
public:
  using GV = _GV;
  using RF = double;
  using ctype = typename GV::Grid::ctype;
  static constexpr int dim = GV::dimension;
  using DomainCFG = Dune::UDG::DomainConfiguration<GV, LGV>;
  using ST = Dune::UDG::SimpleTpmcTriangulation<GV, LGV>;
  using LFE = Dune::Q1LocalFiniteElement<RF,RF,dim>;
  using VST = VirtualSubTriangulation<GV>;
  using FEMTraits = Dune::PDELab::UnfittedFiniteElementMapTraits<LFE, typename ST::EntityPart>;
  using Q1 = Dune::PDELab::UnfittedFiniteElementMap<FEMTraits, VST>;
  using CON = PDELab::NoConstraints;
  using GFS = PDELab::GridFunctionSpace<GV,Q1,CON>;
  using Param = CutCell::TestProblem<GV,RF>;
  using EdgeNorm = StructuredGridEdgeNormProvider; // MultiEdgeNormProvider;
  using Weights = ConstantPenaltyFluxWeights; // UnfittedDynamicPenaltyFluxWeights;
  using LOP = ConvectionDiffusion_DG_LocalOperator<Param,EdgeNorm,Weights>;
  using V = PDELab::Backend::Vector<GFS,RF>;
  using UST = PDELab::UnfittedSubTriangulation<GV>;
  using MBE = PDELab::ISTL::BCRSMatrixBackend<>;
  using GO = UDG::UDGGridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,UST>;

  using Domain = FieldVector<ctype, dim>;
  using Fnkt = Functions::GridViewFunction<RF(Domain), LGV>;

private:

  static
  DomainCFG createDomainConfiguration(Fnkt & lv)
  {
    DomainCFG dom;
    dom.addDomain(Dune::UDG::Domain(0, {"i"}));
    dom.addInterface(Dune::UDG::Interface<LGV>(0, lv));
    return std::move(dom);
  }

public:

  template<typename F>
  CutFEMModel(const GV& gv, const LGV& lgv, F && lset, double h) :
    _gv(gv), _lgv(lgv),
    _lv(Functions::makeAnalyticGridViewFunction(lset, _lgv)),
    _domainConfiguration(createDomainConfiguration(_lv)),
    _triangulation(_gv, _lgv, _domainConfiguration),
    _q1fem(_lfe, _triangulation),
    _gfs(_gv,_q1fem,_con),
    _edgeNorm(h),
    _ust(_gv, _triangulation)
  {
    // build function space
    _gfs.name("u");

    // local operator
    _lop = std::make_shared<LOP>(
      _param, _edgeNorm, _weights,
      ConvectionDiffusion_DG_Scheme::fromString("sipg"),
      4.0 /* penalty */);

    // grid operator
    _go = std::make_shared<GO>(_gfs,_gfs,_ust,*_lop,MBE(15));
  }

  void selectVersion(int i)
  {
    _gfs.name(std::string("u" + std::to_string(i)));
    _lop->selectVersion(i);
  }

  ST& triangulation()
  {
    return _triangulation;
  }

  GFS& gfs()
  {
    return _gfs;
  }

  GO& go()
  {
    return *_go;
  }

  GV& gv()
  {
    return _gv;
  }

  V emptyVector()
  {
    return V(_gfs,0.0);
  }

  CutFEMModel( const CutFEMModel & ) = delete;

  template<typename VTKW>
  void addDomainToVTKWriter(VTKW & vtkwriter) const
  {
    VTK::FieldInfo info_l("levelset", VTK::FieldInfo::Type::scalar,1);
    vtkwriter.addVertexData(_lv, info_l);
  }

private:
  GV _gv;
  LGV _lgv;
  Fnkt _lv;
  DomainCFG _domainConfiguration;
  ST _triangulation;
  LFE _lfe;
  CON _con;
  Q1 _q1fem;
  GFS _gfs;
  Param _param;
  EdgeNorm _edgeNorm;
  Weights _weights;
  std::shared_ptr<LOP> _lop;
  UST _ust;
  std::shared_ptr<GO> _go;
};

} // end namespace CutCell

} // end namespace Dune

#endif // CUTCELL_CUTFEM_MODEL_HH
