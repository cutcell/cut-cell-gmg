#ifndef CUTCELL_INTERPOLATION_HH
#define CUTCELL_INTERPOLATION_HH

#include <dune/pdelab/function/discretegridviewfunction.hh>

namespace Dune {
namespace CutCell {

/** \brief construct an interpolation operator from
    model D1 to model D2
 */
template <typename D1, typename D2>
class InterpolationOperator:
  public LinearOperator<PDELab::Backend::Native<typename D1::V>,PDELab::Backend::Native<typename D2::V>>
{
  using X = PDELab::Backend::Native<typename D1::V>;
  using Y = PDELab::Backend::Native<typename D2::V>;
  using Base = LinearOperator<X,Y>;
public:

  using field_type = typename Base::field_type;

  InterpolationOperator(D1 & d1, D2 & d2) :
    _d1(d1), _d2(d2), _hs(_d1.gv())
  {
  }

  void apply (const X& x, Y& y) const
  {
    using GFS1 = typename D1::GFS;
    using V1 = typename D1::V;
    using V2 = typename D2::V;
    // wrap data into highlevel vectors
    V1 v(_d1.gfs(),const_cast<X&>(x));
    V2 u(_d2.gfs(),y);
    Dune::PDELab::DiscreteGridFunction<GFS1,V1> f(_d1.gfs(),v);
    PDELab::interpolate([&](const auto &x) {
        auto e = _hs.findEntity(x);
        Dune::FieldVector<double,1> res;
        f.evaluate(e,e.geometry().local(x),res);
        return res[0];
      },_d2.gfs(),u);
  }

  //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
  void applyscaleadd (field_type alpha, const X& x, Y& y) const
  {
    assert(false);
  }

private:
  D1 & _d1;
  D2 & _d2;
  using Grid = typename D1::GV::Grid;
  using IS = typename D1::GV::IndexSet;
  HierarchicSearch<Grid,IS> _hs;
};

} // end namespace CutCell
} // end namespace Dune

#endif // CUTCELL_INTERPOLATION_HH
