#ifndef DUNE_PDELAB_MULTIGRID_PROLONGATIONASSEMBLER_HH
#define DUNE_PDELAB_MULTIGRID_PROLONGATIONASSEMBLER_HH

#include <cassert>
#include <dune/common/dynmatrix.hh>
#include <dune/common/dynvector.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/common/version.hh>
#include <dune/pdelab/gridoperator/common/localmatrix.hh>
#include <dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>

namespace Dune {
  namespace PDELab {
    namespace Multigrid {
      template <class LFS, class E>
      class CoarseInFineFunction {
        typedef typename LFS::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFS::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef FieldVector<DF, E::dimension> DomainType;
      public:

        struct Traits
        {
          typedef typename LFS::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
          typedef FieldVector<DF, E::dimension> DomainType;
        };

        CoarseInFineFunction(const E& element, const LFS& lfsCoarse, int basisIndex)
          : element_(element), lfsCoarse_(lfsCoarse), basisIndex_(basisIndex) {
        }

        void evaluate(const DomainType& x,
          RangeType& y) const {
          // map fine to coarse
          DomainType xif = element_.geometryInFather().global(x);
          std::vector<RangeType> v;
          // evaluate in coarse
          lfsCoarse_.finiteElement().localBasis().evaluateFunction(xif, v);
          y = v[basisIndex_];
        }
      private:
        // fineElement we are working on
        const E& element_;
        // coarse localfunctionspace
        const LFS& lfsCoarse_;
        // index of the coarse basis function
        const int basisIndex_;
      };

      template <class GFS, class MB, typename FT>
      struct ProlongationAssemblerTraits {
        typedef typename MB::size_type SizeType;
        typedef GFS GridFunctionSpaceType;
        typedef GFS TestGridFunctionSpace;
        typedef GFS TrialGridFunctionSpace;
        typedef LocalFunctionSpace<GFS> LocalFunctionSpaceType;
        typedef FT FieldType;
        typedef typename GFS::Traits::GridViewType GridViewType;
        typedef Backend::Vector<GFS,FT> VectorType;
        typedef MB MatrixBackend;
        typedef Backend::Matrix<MB, VectorType, VectorType, FT> MatrixType;
        typedef typename GridViewType::template Codim<0>::Entity EntityType;
      };

      template <class M, class E>
      struct ProlongationLocalAssembler
        : public TypeTree::TreePairVisitor
        , public TypeTree::DynamicTraversal {

        template <typename LFS, typename TreePath>
        void leaf(const LFS& lfsCoarse, const LFS& lfsFine, TreePath treepath) const {
          typedef typename LFS::Traits::FiniteElementType::
            Traits::LocalBasisType::Traits::DomainFieldType DF;
          typedef std::size_t SizeType;

          for (SizeType coarseIndex = 0; coarseIndex < lfsCoarse.size(); coarseIndex++) {
            std::vector<DF> v;
            CoarseInFineFunction<LFS, E> ciff(e_, lfsCoarse, coarseIndex);
            lfsFine.finiteElement().localInterpolation().interpolate(ciff, v);
            for (SizeType fineIndex = 0; fineIndex < lfsFine.size(); ++fineIndex) {
              m_.accumulate(lfsCoarse, coarseIndex, lfsFine, fineIndex, v[fineIndex]);
            }
          }
        }

        ProlongationLocalAssembler(M& m, const E& e)
          : m_(m), e_(e) {
        }
      private:
        M& m_;
        const E& e_;
      };

      template <class GFS, class MB, typename FT>
      class ProlongationAssembler {
      public:
        typedef ProlongationAssemblerTraits<GFS,MB,FT> Traits;

        ProlongationAssembler(const GFS& gfsFine, const GFS& gfsCoarse)
          : gfsFine_(gfsFine), gfsCoarse_(gfsCoarse),
            lfsFine_(gfsFine), lfsCoarse_(gfsCoarse),
            backend_(10)
        {
          assert(gfsFine_.size() >= gfsCoarse_.size());
        }

        void assemble(typename Traits::MatrixType &result) {
          typedef typename Traits::GridViewType::template Codim<0>::Iterator Iterator;
          const typename Traits::GridViewType& gvFine = gfsFine_.gridView();
          typedef typename Traits::EntityType ET;
          unsigned int eindex = 0;
          // loop through all fine Elements
          for (Iterator it = gvFine.template begin<0>();
               it != gvFine.template end<0>(); ++it, ++eindex) {
            lfsFine_.bind(*it);
            lfsCoarse_.bind(it->father());
            typedef LocalMatrix<typename Traits::FieldType, typename Traits::FieldType> LM;
            LM lm(lfsCoarse_.size(), lfsFine_.size(), 0.0);
            typedef typename LM::WeightedAccumulationView WAV;
            WAV wav = lm.weightedAccumulationView(1.0);
            ProlongationLocalAssembler<WAV, ET> la(wav, *it);
            TypeTree::applyToTreePair(lfsCoarse_, lfsFine_, la);
            for (typename Traits::SizeType i = 0; i<lfsCoarse_.size(); ++i)
              for (typename Traits::SizeType j = 0; j<lfsFine_.size(); ++j)
                result(gfsCoarse_.ordering().mapIndex(lfsCoarse_.dofIndex(i)),
                  gfsFine_.ordering().mapIndex(lfsFine_.dofIndex(j)))
                  = lm(lfsCoarse_, i, lfsFine_, j);
          }
        }

        const GFS& trialGridFunctionSpace() const {
          return gfsFine_;
        }

        const GFS& testGridFunctionSpace() const {
          return gfsCoarse_;
        }

        template<typename Pattern>
        void fill_pattern(Pattern& pattern) const {
          typedef typename Traits::GridViewType::template Codim<0>::Iterator Iterator;
          const typename Traits::GridViewType& gvFine = gfsFine_.gridView();
          // loop through all fine Elements
          int i = 0;
          for (Iterator it = gvFine.template begin<0>();
               it != gvFine.template end<0>(); ++it, ++i) {
            lfsFine_.bind(*it);
            lfsCoarse_.bind(it->father());
            // loop through all local dofs
            for (typename Traits::SizeType j = 0; j<lfsFine_.size(); ++j) {
              for (typename Traits::SizeType k = 0; k<lfsCoarse_.size(); ++k) {
                pattern.add_link(gfsCoarse_.ordering().mapIndex(lfsCoarse_.dofIndex(k)),
                  gfsFine_.ordering().mapIndex(lfsFine_.dofIndex(j)));
              }
            }
          }
        }

        //! Get the matrix backend for this grid operator.
        const typename Traits::MatrixBackend& matrixBackend() const
        {
          return backend_;
        }

      private:
        const GFS& gfsFine_;
        const GFS& gfsCoarse_;
        mutable typename Traits::LocalFunctionSpaceType lfsFine_;
        mutable typename Traits::LocalFunctionSpaceType lfsCoarse_;
        MB backend_;
      };
    }
  }

  template<class M, class X, class Y>
  class TransposedMatrixAdapter : public AssembledLinearOperator<M,X,Y>
  {
  public:
    //! export types
    typedef M matrix_type;
    typedef X domain_type;
    typedef Y range_type;
    typedef typename X::field_type field_type;

    //! constructor: just store a reference to a matrix
    explicit TransposedMatrixAdapter (const M& A) : _A_(A) {}

    //! apply operator to x:  \f$ y = A(x) \f$
    virtual void apply (const X& x, Y& y) const
    {
      _A_.mtv(x,y);
    }

    //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
    virtual void applyscaleadd (field_type alpha, const X& x, Y& y) const
    {
//      _A_.usmtv(alpha,x,y);
    }

    //! get matrix via *
    virtual const M& getmat () const
    {
      return _A_;
    }

    //! Category of the solver (see SolverCategory::Category)
    virtual SolverCategory::Category category() const
    {
      return SolverCategory::sequential;
    }

  private:
    const M& _A_;
  };

}

#endif //DUNE_PDELAB_MULTIGRID_PROLONGATIONASSEMBLER_HH
