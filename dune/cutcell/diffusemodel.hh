#ifndef CUTCELL_DIFFUSE_MODEL_HH
#define CUTCELL_DIFFUSE_MODEL_HH

#include "diffuse.hh"

#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>

#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/constraints/conforming.hh>

namespace Dune {

namespace PDELab {
  struct AllDirichlet :
    public Dune::PDELab::DirichletConstraintsParameters
  {
    template<typename I>
    bool isDirichlet(const I & ig,
      const Dune::FieldVector<typename I::ctype, I::mydimension> & coord
      ) const
    {
      return true;
    }
  };
}

namespace CutCell {

template<typename _GV>
class DiffuseModel
{
public:
  using GV = _GV;
  using RF = double;
  using ctype = typename GV::Grid::ctype;
  static constexpr int dim = GV::dimension;
  using Q1 = PDELab::QkLocalFiniteElementMap<GV,RF,RF,1>;
  using CON = PDELab::ConformingDirichletConstraints;
  using GFS = PDELab::GridFunctionSpace<GV,Q1,CON>;
  using Param = CutCell::DiffuseInterfaceParameters<GV,RF>;
  using LOP = CutCell::DiffuseInterfacePoisson<Param,Q1>;
  using V = PDELab::Backend::Vector<GFS,RF>;
  using MBE = PDELab::ISTL::BCRSMatrixBackend<>;
  using CC = typename GFS::template ConstraintsContainer<RF>::Type;
  using GO = PDELab::GridOperator<GFS,GFS,LOP,MBE,RF,RF,RF,CC,CC>;

  using Domain = FieldVector<ctype, dim>;
  using Fnkt = Functions::GridViewFunction<RF(Domain), GV>;

  template<typename F>
  DiffuseModel(const GV& gv, F && lset, double epsilon, int intorder) :
    _gv(gv), _q1fem(_gv), _gfs(_gv,_q1fem,_con)
  {
    // build function space
    _gfs.name("u1");

    _lv = Functions::makeAnalyticGridViewFunction(
      lset, gv);

    // diffuse domain
    _dom = Functions::makeAnalyticGridViewFunction(
      [this,epsilon](auto x)
      {
        using std::tanh;
        return 0.5 * (1.0 - tanh(3.0*this->_lv(x)/epsilon));
      },
      _gv);

    // diffuse interface
    _itf = Functions::makeAnalyticGridViewFunction(
      [this](auto x)
      {
        auto d = this->_dom(x);
        return 36 * d*d*(1.0-d)*(1.0-d);
      },
      gv);
    // dirichlet extension
    auto g = Functions::makeAnalyticGridViewFunction(
      [](auto x)
      {
        return 0.0;
      },
      gv);

    // source function
    auto f = Functions::makeAnalyticGridViewFunction(
      [](auto x)
      {
        return 1.0;
      },
      gv);

    _param = std::make_shared<Param>(localFunction(_lv),
      localFunction(_dom),
      localFunction(_itf),
      localFunction(g),
      localFunction(f),
      epsilon,intorder);

    // local operator
    _lop = std::make_shared<LOP>(*_param);

    // make constraints map and initialize it from a function and ghost
    _cc.clear();
    PDELab::AllDirichlet allDirichlet;
    PDELab::constraints(
      allDirichlet,
      _gfs,_cc,false);

    // grid operator
    _go = std::make_shared<GO>(_gfs,_cc,_gfs,_cc,*_lop,MBE(10));
  }

  void selectVersion(int i)
  {
    _gfs.name(std::string("u" + std::to_string(i)));
    _lop->selectVersion(i);
  }

  GFS& gfs()
  {
    return _gfs;
  }

  GO& go()
  {
    return *_go;
  }

  GV& gv()
  {
    return _gv;
  }

  V emptyVector()
  {
    return V(_gfs,0.0);
  }

  DiffuseModel( const DiffuseModel & ) = delete;

  template<typename VTKW>
  void addDomainToVTKWriter(VTKW & vtkwriter) const
  {
    VTK::FieldInfo info_l("levelset", VTK::FieldInfo::Type::scalar,1);
    VTK::FieldInfo info_d("domain",   VTK::FieldInfo::Type::scalar,1);
    VTK::FieldInfo info_i("interface",VTK::FieldInfo::Type::scalar,1);
    vtkwriter.addVertexData(_lv, info_l);
    vtkwriter.addVertexData(_dom,info_d);
    vtkwriter.addVertexData(_itf,info_i);
  }

private:
  GV _gv;
  Q1 _q1fem;
  CON _con;
  GFS _gfs;
  std::shared_ptr<Param> _param;
  std::shared_ptr<LOP> _lop;
  CC _cc;
  std::shared_ptr<GO> _go;

  Fnkt _lv;
  Fnkt _dom;
  Fnkt _itf;
};

} // end namespace CutCell

} // end namespace Dune

#endif // CUTCELL_DIFFUSE_MODEL_HH
