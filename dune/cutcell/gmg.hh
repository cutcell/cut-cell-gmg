#ifndef CUTCELL_GMG_HH
#define CUTCELL_GMG_HH

// add your classes here

namespace Dune {
namespace CutCell {

  template<typename V>
  class GMG :
    public Preconditioner<V,V>
  {
  public:

    GMG(
      std::vector<unsigned int> sizes,
      std::vector<std::shared_ptr<LinearOperator<V,V>>> & p,
      std::vector<std::shared_ptr<LinearOperator<V,V>>> & r,
      std::vector<std::shared_ptr<Preconditioner<V,V>>> & s,
      std::shared_ptr<InverseOperator<V,V>> & c,
      std::vector<std::shared_ptr<LinearOperator<V,V>>> o
      ) :
      _gamma(1),
      _prolongation(p),
      _restriction(r),
      _smoother(s),
      _coarseSolver(c),
      _operator(o),
      _sizes(sizes)
    {
      // allocate vector hierarchy
      _x.resize(levels());
      _d.resize(levels());
      _v.resize(levels());
      for (int l=0; l<levels(); l++)
      {
        _x[l].resize(_sizes[l]);
        _d[l].resize(_sizes[l]);
        _v[l].resize(_sizes[l]);
      }
    }

    //! Category of the preconditioner (see SolverCategory::Category)
    virtual SolverCategory::Category category() const
    {
      return SolverCategory::sequential;
    }

    virtual void pre (V& x, V& b)
    {}

    virtual void apply (V& x, const V& d)
    {
      _d.back() = d;
      levelApply(maxLevel(),x,_d.back());
    }

    virtual void post (V& x) {}

  private:

    int maxLevel() const { return _sizes.size()-1; }
    int levels() const { return _sizes.size(); }

    void levelApply(int l, V& x, V& d)
    {
      // some shortcuts to level variables
      auto & v = _v[l];

      // coarse level solve
      if (l == 0)
      {
        // solve coarse mesh directly
        InverseOperatorResult _res;
        // call solver
        // std::cout << "coarse solve level 0" << std::endl;
        _coarseSolver->apply(x,d,_res); // the solver modifies the rhs, but we don't need it anymore, so we don't care...
        return;
      }
      else
      {
        auto & op = *_operator[l];
        auto & restriction = *_restriction[l-1];
        auto & prolongation = *_prolongation[l];
        auto & smoother = *_smoother[l];
        /* pre smoothing */
        // for (int i = 0; i < _pre; i++)
        {
          // reset update vector
          v = 0;
          // std::cout << "pre smooth level " << l << std::endl;
          // apply smoother
          smoother.apply(v,d); // smoother does not change the rhs, we are happy :-)
          // update solution
          x += v;
          // update defect
          op.applyscaleadd(-1,v,d);  // update defect
        }

        /* next level (either V or W cycle) */
        for (int i = 0; i < _gamma; i++)
        {
          v = 0;
          _x[l-1] = 0;
          // restrict defect
          // std::cout << "restrict from level " << l << std::endl;
          restriction.apply(d,_d[l-1]);
          // apply coarser level
          levelApply(l-1,_x[l-1],_d[l-1]);
          // prolongate update
          // std::cout << "prolongate to level " << l << std::endl;
          prolongation.apply(_x[l-1],v);
          // update solution
          x += v;
          // update defect
          op.applyscaleadd(-1,v,d);
        }

        /* post smoothing */
        // for (int i = 0; i < _post; i++)
        {
          // reset update vector
          v = 0;
          // apply smoother
          smoother.apply(v,d); // smoother does not change the rhs, we are happy :-)
          // update solution
          x += v;
          // update defect
          op.applyscaleadd(-1,v,d);  // update defect
        }
      }
    }

    // V or W cycle
    // int _pre;
    int _gamma;
    // int _post;
    // operator hierarchy
    std::vector<std::shared_ptr<LinearOperator<V,V>>> _prolongation;
    std::vector<std::shared_ptr<LinearOperator<V,V>>> _restriction;
    std::vector<std::shared_ptr<Preconditioner<V,V>>> _smoother;
    std::shared_ptr<InverseOperator<V,V>> _coarseSolver;
    std::vector<std::shared_ptr<LinearOperator<V,V>>> _operator;
    // vector hierarchies
    std::vector<unsigned int> _sizes;

    std::vector<V> _x; // solution vectors on the different levels
    std::vector<V> _d; // defect vectors on the different levels
    std::vector<V> _v; // update vectors on the different levels
  };

} // end namespace CutCell
} // end namespace Dune

#endif // CUTCELL_GMG_HH
