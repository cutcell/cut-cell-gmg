// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:
#ifndef DUNE_PDELAB_BACKEND_ISTL_GENERICSOLVER_HH
#define DUNE_PDELAB_BACKEND_ISTL_GENERICSOLVER_HH

#include <dune/pdelab/backend/istl/seqistlsolverbackend.hh>

namespace Dune {
  namespace PDELab {
  namespace ISTL {

    //! \addtogroup Backend
    //! \ingroup PDELab
    //! \{

    template<typename X, typename Y>
    struct SolverFactory
    {
      using scalar_real_type = typename InverseOperator<X,Y>::scalar_real_type;
      virtual std::shared_ptr<InverseOperator<X,Y>>
      create (LinearOperator<X,Y>& op, Preconditioner<X,Y>& prec, scalar_real_type reduction, int maxit, int verbose) = 0;
    };

    template<typename Solver>
    struct SolverFactoryImp :
      public SolverFactory<typename Solver::domain_type, typename Solver::range_type>
    {
      using X = typename Solver::domain_type;
      using Y = typename Solver::range_type;
      using scalar_real_type = typename InverseOperator<X,Y>::scalar_real_type;
      virtual std::shared_ptr<InverseOperator<X,Y>>
      create (LinearOperator<X,Y>& op, Preconditioner<X,Y>& prec, scalar_real_type reduction, int maxit, int verbose)
      {
        return std::make_shared<Solver>(op, prec, reduction, maxit, verbose);
      }
    };

    template<typename GO, typename RF = double>
    class GenericSolver
      : public SequentialNorm, public LinearResultStorage
    {
    public:
      using M = typename GO::Traits::Jacobian;
      using V = typename GO::Traits::Domain;
      using W = typename GO::Traits::Range;
      using Prec = Preconditioner<Backend::Native<V>, Backend::Native<W>>;
      using SF = SolverFactory<Backend::Native<V>, Backend::Native<W>>;

      /*! \brief make a linear solver object

        \param[in] maxiter_ maximum number of iterations to do
        \param[in] verbose_ print messages if true
      */
      template<typename S>
      explicit GenericSolver(MetaType<S> solvertype,
        std::shared_ptr<Prec> prec,
        unsigned maxiter=5000, int verbose=1)
        : _sf(std::make_shared<SolverFactoryImp<S>>()),
          _prec(prec),
          _maxiter(maxiter), _verbose(verbose)
      {}

      /*! \brief solve the given linear system

        \param[in] A the given matrix
        \param[out] z the solution vector to be computed
        \param[in] r right hand side
        \param[in] reduction to be achieved
      */
      void apply(M& A, V& z, W& r, typename Dune::template FieldTraits<typename W::ElementType>::real_type reduction)
      {
        using Backend::Native;
        using Backend::native;

        Dune::MatrixAdapter<Native<M>,
                            Native<V>,
                            Native<W>> opa(native(A));
        auto solver = _sf->create(opa, *_prec, reduction, _maxiter, _verbose);
        Dune::InverseOperatorResult stat;
        solver->apply(native(z), native(r), stat);
        res.converged  = stat.converged;
        res.iterations = stat.iterations;
        res.elapsed    = stat.elapsed;
        res.reduction  = stat.reduction;
        res.conv_rate  = stat.conv_rate;
      }

    private:
      std::shared_ptr<SF> _sf;
      std::shared_ptr<Prec> _prec;
      unsigned _maxiter;
      int _verbose;
    };

    //! \} group Sequential Solvers
    //! \} group Backend

  } // namespace ISTL
  } // namespace PDELab
} // namespace Dune

#endif // DUNE_PDELAB_BACKEND_ISTL_GENERICSOLVER_HH
